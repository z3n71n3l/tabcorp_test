<?php
/**
 * Controller for Totes
 * 
 * @author stephan
 */
class controller_Totes {
	
    /**
     * request 
     * 
     * @var array
     */
    private $_request;
    
	/**
	 * construct
	 * 
	 * @param array $request - The request variable
	 * 
	 * @return void
	 */
	public function __construct($request) {
	    $this->_request = $request;
	}
	
	/**
	 * action totes
	 */
	public function totesCalculator() {
	    $args = array();
	    $this->render(__FUNCTION__);
	}
	
	/**
	 * action totes
	 */
	public function calcDividends() {
	    $bets = $this->getParam('bets');
	    $outcome = $this->getParam('outcome');
	    
	    $model = new model_Model();
	    $result['dividends'] = $model->calcDividends($bets, $outcome);
	    $result['success'] = 1;
	    $this->render(__FUNCTION__, $result);
	}
	
	private function render($view, $result=array()) {
	    $request = $this->_request;
	    $result = $result;
	    require_once 'view/' . $view.'.php';
	}
	
	/**
	 * Get param from request
	 * 
	 * @param string $key - index key
	 * 
	 * @return String
	 */
	private function getParam($key) {
	    if (!isset($this->_request[$key])) {
	        return null;
	    }
	    return $this->_request[$key];
	}
}