<?php

//@todo remove debugging only
// error_reporting(E_ALL);
// ini_set('display_error', 1);

ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR 
    . dirname(__FILE__) . DIRECTORY_SEPARATOR
);

function __autoload($class)
{
    $parts = str_replace('_', '/', $class);
    require $parts . '.php';
}

$route = null;

if (isset($_REQUEST['route'])) {
    $route = $_REQUEST['route'];
    if ($_REQUEST['route'] == 'Totes' && isset($_REQUEST['action'])) {
        //get action name
        $action = trim($_REQUEST['action']);
        
        //remove unwanted params
        unset($_REQUEST['action'], $_REQUEST['route']);
        
        //get the only controller
        $controller = new controller_Totes($_REQUEST);
        //call action
        $controller->$action();
    }
}

