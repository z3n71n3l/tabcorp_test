<?php

/**
 * database handler
 * 
 * @author stephan
 */
class db {
	
	private $_user = null;
	private $_pass = null;
	private $_dbname = null;
	private $_port = null;
	private $_config = null;
	
	/**
	 * @var mysql_connect
	 */
	private $_db = null;
	
	/**
	 * construct
	 * 
	 * @param String $hostNameKey - name of host
	 * @param String $dnConfigRefName - 
	 * 
	 * @return void
	 */
	public function __construct($dnConfigRefName = null) {
		//get configurations
		$this->_config = Singleton::get()->getConfigDb();
		//set class attributes required to connect
		$this->setConnectionConfig($dnConfigRefName);
	}
	
	/**
	 * Set the config array for the database 
	 * 
	 * @param String $dnConfigRefName - database config key
	 * 
	 * @throws Exception - When database is not configured correctly.
	 * 
	 * @return void
	 */
	private function setConnectionConfig($dnConfigRefName) {
		$conf = Singleton::get()->getConfigEnfironment();
		
		if (!isset($this->_config[$dnConfigRefName])) {
			throw new Exception('Database with for `'.$dnConfigRefName.'` is not configured.');
		}
		
		//@todo add isset check for missing index
		$this->_user = $this->_config[$dnConfigRefName]['username'];
		$this->_pass = $this->_config[$dnConfigRefName]['pass'];
		$this->_dbname = $this->_config[$dnConfigRefName]['name'];
		$this->_port = $this->_config[$dnConfigRefName]['port'];
		
		//sanity check
		if (empty($this->_dbname) || empty($this->_port) || empty($this->_hostName)) {
			throw new Exception('Database with for `'.$dnConfigRefName.'` is not configured.');
		}
	}
	
	
	/**
	 * get connection to tunneled 
	 * 
	 * @throws Exception
	 * 
	 * @return void
	 */
	private function setDb($sshTunnel = false) {
		//create db connection
		$this->_db = mysql_connect('127.0.0.1:' . $this->_port, $this->_user, $this->_pass);
		
		if (!$this->_db) {
			throw new Exception('Could not connect to specified database `' . $this->_dbname . '`; using: `127.0.0.1:'.$this->_port . '`;' . $this->_user . '; ' . $this->_pass);
		}
		//set database name
		mysql_select_db($this->_dbname, $this->_db);
	}
	
	/**
	 * @return mysql_connect
	 */
	public function getDb() {
		if (!isset($this->_db)) {
			$this->setDb();
		}
		return $this->_db;
	}
}
?>
