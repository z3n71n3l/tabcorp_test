<?php 

/**
 * Singleton class
 */
final class system_Singleton
{
    /**
     * @var Singleton
     */
	protected static $instance = null;
	
	/**
	 * @var mysql_connect
	 */
	private $_db = null;
	
	/**
	 * config list
	 * 
	 * @var array
	 */
	private $_config = null;
	
	protected function __construct() {}
	protected function __clone() {}
	
	/**
	 * @return Singleton
	 */
	public static function get() {
		if (!isset(static::$instance)) {
			static::$instance = new static;
		}
		return static::$instance;
	}
	
	/**
	 * @param string $section - part of the config to get
	 * 
	 * @return array
	 */
	private function getConfig($section=null) {
		if (!isset($this->_config)) {
			//get configurations
			$config = new system_Config();
			$this->_config = $config->getConfig();
		}

		if ($section) {
			return $this->_config[$section];
		}
		
		return $this->_config;
	}
	
	public function getConfigComission() {
		return $this->getConfig('comission');
	}
	
}