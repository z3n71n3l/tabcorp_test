<?php

/**
 * config object
 * 
 * @author stephan
 */
class system_Config {
	
	/**
	 * Retrieves a part of the config.
	 * 
	 * @param string $section - section title in the config
	 * 
	 * @return array
	 */
	public function getConfig($section=null) {
		$config = parse_ini_file('config.ini',true, INI_SCANNER_RAW);
		if (isset($section) && !isset($config[$section])) {
			die('This config section is not supported');
		}
		
		return isset($section) ? $config[$section] : $config;
	}
}