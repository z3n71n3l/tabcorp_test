<?php

//@todo remove debugging only
// error_reporting(E_ALL);
// ini_set('display_error', 1);

ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR 
    . dirname(__FILE__) . DIRECTORY_SEPARATOR
);

function __autoload($class)
{
    $parts = str_replace('_', '/', $class);
    require $parts . '.php';
}

$route = null;

$_REQUEST['cli'] = true;

foreach ($argv as $arg) {
    if (strpos($arg, '--') !== false) {
        list($name, $val) = explode('=', $arg);
        $key = substr($name, 2);
        if ($key == 'args') {
            $wagers = explode(',', $val);
            foreach ($wagers as $wager) {
                $wager = strtoupper($wager);
                if (strpos($wager, 'BET') !== false) {
                    list($null, $product,$selection, $stake) = explode(':', $wager);
                    $_REQUEST['bets'][] = array(
                        'product' => $product,
                        'selection' => $selection,
                        'stake' => $stake
                    );
                }
                if (strpos($wager, 'RESULT') !== false) {
                    $tempResult = explode(':', $wager);
                    array_shift($tempResult);
                    
                    $_REQUEST['outcome'] = array(
                        1 => array_shift($tempResult),
                        2 => array_shift($tempResult),
                        3 => array_shift($tempResult),
                    );
                }
            }
        } else {
            $_REQUEST[$key] = $val;
        }
    }
}

if (isset($_REQUEST['route'])) {
    $route = $_REQUEST['route'];
    if ($_REQUEST['route'] == 'Totes' && isset($_REQUEST['action'])) {
        //get action name
        $action = trim($_REQUEST['action']);
        
        //remove unwanted params
        unset($_REQUEST['action'], $_REQUEST['route']);
        
        //get the only controller
        $controller = new controller_Totes($_REQUEST);
        //call action
        $controller->$action();
    }
}