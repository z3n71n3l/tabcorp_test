<!DOCTYPE html>
<?php


?>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
    	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js" /></script>
    	<script type="text/javascript">
    		var bets = [
{product:'W',selection: 1, stake: 3},
{product:'W',selection: 2, stake: 4},
{product:'W',selection: 3, stake: 5},
{product:'W',selection: 4, stake: 5},
{product:'W',selection: 1, stake: 16},
{product:'W',selection: 2, stake: 8},
{product:'W',selection: 3, stake: 22},
{product:'W',selection: 4, stake: 57},
{product:'W',selection: 1, stake: 42},
{product:'W',selection: 2, stake: 98},
{product:'W',selection: 3, stake: 63},
{product:'W',selection: 4, stake: 15},

{product:'P',selection: 1, stake: 31},
{product:'P',selection: 2, stake: 89},
{product:'P',selection: 3, stake: 28},
{product:'P',selection: 4, stake: 72},
{product:'P',selection: 1, stake: 40},
{product:'P',selection: 2, stake: 16},
{product:'P',selection: 3, stake: 82},
{product:'P',selection: 4, stake: 52},
{product:'P',selection: 1, stake: 18},
{product:'P',selection: 2, stake: 74},
{product:'P',selection: 3, stake: 39},
{product:'P',selection: 4, stake: 105}
	    		];
    		
			$(document).ready(function(){
				for (var i in bets) {
					var product = bets[i].product,
    					selection = bets[i].selection,
    					stake = bets[i].stake;
					$('#tblBet tr:last').after(
						'<tr><td>' + product + '</td>' + '<td>' + selection + '</td>' + '<td>' + stake + '</td></tr>'
					);
				}
				//place bet
				$("#btnPlaceBet").click(function(){
					var product = $("#product").val(),
						selection = $("#selection").val(),
						stake = $("#stake").val();

					$('#tblBet tr:last').after(
						'<tr><td>' + product + '</td>' + '<td>' + selection + '</td>' + '<td>' + stake + '</td></tr>'
					);
					bets.push({product:product,
						selection:selection,
						stake:stake});

				});
				
				//remove last bet
				$("#btnClearLastBet").click(function(){
					if (bets.length <=0 ) {
						return;
					}
					bets.pop();
					$('#tblBet tr:last').remove();
				});
				
				//set results and calculate dividends
				$("#btnSetResults").click(function(){
					var outcome = {
						'1' : $("#outcome_1").val(),
						'2' : $("#outcome_2").val(),
						'3' : $("#outcome_3").val()
					};

					var data = {
						route : 'Totes',
						action : 'calcDividends',
						bets: bets,
						outcome: outcome
					};
					$.post({
						type: "POST",
						url: '<?php echo $_SERVER['SCRIPT_NAME'] ?>',
						data: data,
						success: function(response) {
							if (response.success == 1) {
								$('[name="trDividends"]').remove();

								var winDividend = response.dividends.win;
								
								$('#tblDividends tr:last').after(
									'<tr name="trDividends"><td>Win</td>' + '<td>' + winDividend.horseNr + '</td>' + '<td>$' + winDividend.dividend + '</td></tr>'
								);

								var count = Object.keys(response.dividends.place).length;

								for (var i in response.dividends.place) {
									var placeDividend = response.dividends.place[i];
									$('#tblDividends tr:last').after(
										'<tr name="trDividends"><td>Place</td>' + '<td>' + placeDividend.horseNr + '</td>' + '<td>$' + placeDividend.dividend + '</td></tr>'
									);
								}
							}
						},
						dataType: "json"
					});
				});
			});
			
    	</script>
    </head>
    <body>
    	<h4>Totes Calculator</h4>
    	<hr/>
    	<h5>Place a bet</h5>
    		
		<select id="product">
        	<option value="W">Win</option>
        	<option value="P">Place</option>
        </select>
		<input type="text" id="selection" value="" placeholder="<?php echo htmlentities('<selection>'); ?>" />
		<input type="text" id="stake"     value="" placeholder="<?php echo htmlentities('<stake>'); ?>" />
        <br />
        <button id="btnPlaceBet">Place bet</button>
        <button id="btnClearLastBet">Clear last bet</button>
        <br />
        <br />
        <strong>Bet slip</strong>
    	<table id="tblBet" style="border:1px solid" >
    		<tr>
    			<td>Product</td>
    			<td>Selection</td>
    			<td>Stake</td>
			</tr>
    	</table>
        
        
        
    	<hr/>
    	<h5>Set results</h5>
        <input type="text" id="outcome_1" value="" placeholder="<?php echo htmlentities('<first>'); ?>" /> :
        <input type="text" id="outcome_2" value="" placeholder="<?php echo htmlentities('<second>'); ?>" /> :
        <input type="text" id="outcome_3" value="" placeholder="<?php echo htmlentities('<third>'); ?>" /> <br/>
        <button id="btnSetResults">Set results</button>
        
        
    	<hr/>
    	<h5>Dividens to pay</h5>
    	<table id="tblDividends" style="border:1px solid" >
    		<tr>
    			<th>product</th>
    			<th>winning Selections</th>
    			<th>dividend</th>
    		</tr>
    		<!-- <tr id="trWin">
        		<td>Win</td>
        		<td>1</td>
        		<td>?</td>
    		</tr>
    		<tr id="trPlaceFirst">
        		<td>Place</td>
        		<td>1</td>
        		<td>?</td>
    		</tr>
    		<tr id="trPlaceSecond">
        		<td>Place</td>
        		<td>2</td>
        		<td>?</td>
    		</tr>
    		<tr id="trPlaceThird">
        		<td>Place</td>
        		<td>3</td>
        		<td>?</td>
    		</tr>-->
    	</table>
    </body>
</html>