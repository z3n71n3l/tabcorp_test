<?php
require_once 'data/Data.php';

class model_Model {
	
    /**
     * @var data_Data
     */
    private $_data;
    
	/**
	 * construct
	 */
	public function __construct($data = null) {
	    
	    if ($data == null) {
	        $data = new data_Data();
	    }
	    $this->setData($data);
	}
	
	/**
	 * Setter for data
	 * 
	 * @param data_Data $data
	 */
	public function setData($data) {
	    $this->_data = $data;
	}
	
	/**
	 * Getter for data
	 * 
	 * @return data_Data
	 */
	public function getData() {
	    return $this->_data;
	}
	
	/**
	 * get all
	 * 
	 * @return array
	 */
	public function calcDividends($bets, $outcome) {
	    $configCommision = system_Singleton::get()->getConfigComission('comission');

	    $totalWinStake = $totalWinPoolStake = $totalPlacePoolStake = 0;
	    $totalPlaceStake = array();
	    //get the winning horse
	    $winningHorse = $outcome['1'];
	    
	    foreach ($bets as $key => $bet) {
	        $product = $bet['product'];
	        $selection = $bet['selection'];
	        $stake = $bet['stake'];
	        
	        if ($product == 'W') {
	            //increase winning pool
	            $totalWinPoolStake += $stake;
	            if ($selection == $winningHorse) {
	                //increase the total amount placed on winning horse
	                $totalWinStake += $stake;
	            }
	        } else if ($product == 'P') {
	            //increase winning pool
	            $totalPlacePoolStake += $stake;
	            
	            //itterate the horses that placed
	            foreach ($outcome as $place => $horseNr) {
	                
	                if (!isset($totalPlaceStake[$place])) {
	                    $totalPlaceStake[$place]['stake'] = 0;
	                    $totalPlaceStake[$place]['horseNr'] = $horseNr;
	                }
	                //check if the horse placed
	                if ($horseNr == $selection) {
        	            $totalPlaceStake[$place]['stake'] += $stake;
        	            $totalPlaceStake[$place]['horseNr'] = $horseNr;
	                }
	            }
	        } // else { do nothing }
	    }

	    //calc win commision
	    $commisionWin = $totalWinPoolStake * $configCommision['win'] / 100;
	    //calc place commision
	    $commisionPlace = $totalPlacePoolStake * $configCommision['place'] / 100;

	    //calc the total used for win pool
	    $poolWinDivivend = ($totalWinPoolStake - $commisionWin) / $totalWinStake;
	    
	    //calc the total to use for each place pool
	    $poolPlaceDividendUpped = ($totalPlacePoolStake - $commisionPlace) / count($outcome)/*number of places*/;

	    $poolPlaceDivivend = array();

	    foreach ($totalPlaceStake as $place => $placeStake) {
	        $poolPlaceDivivend[$place] = array(
	            'horseNr' => $placeStake['horseNr'],
	            //@todo not good to round dividends unless for display
	            'dividend' => round($poolPlaceDividendUpped / $placeStake['stake'], 2)
	        );
	    }
	    if (isset($_REQUEST['cli'])) {
	        /*
	         Win:2:$2.61
	         Place:2:$1.06
	         Place:3:$1.27
	         Place:1:$2.13
	         */
	        echo 'Win:' . $winningHorse . ':' . round($poolWinDivivend,2) . PHP_EOL;
	        foreach ($poolPlaceDivivend as $place) {
    	        echo 'Place:' . $place['horseNr'] . ':$'.$place['dividend'] . PHP_EOL;
	        }
	        die();
	    } else {
    	    return array(
    	        'win' => array(
    	            'horseNr' => $winningHorse,
    	            'dividend' => round($poolWinDivivend,2)//@todo not good to round dividends unless for display
    	        ), 'place' => $poolPlaceDivivend
    	    );
	    }
	}
}